<?php

use App\Http\Controllers\ArticleMainController;
use App\Http\Controllers\ArticleTagController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChangeArticleStatusController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\CoverLoadController;
use App\Http\Controllers\DashBoardController;
use App\Http\Controllers\DownloadsController;
use App\Http\Controllers\GetGuidesController;
use App\Http\Controllers\UploadFilesController;
use App\Http\Controllers\UploadToCloudController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/auth/login', [AuthController::class, 'login']); // получение токена
// загрузка на Яндекс Диск
Route::get('/run-upload-cloud', UploadToCloudController::class); //запуск загрузки в облако

Route::middleware('auth:sanctum')->group(function(){

    // Авторизация
    Route::post('/auth/logout', [AuthController::class, 'logout']); // удаление токена
    Route::get('/auth/user', [AuthController::class, 'userInfo']); // информация о пользователе


    // справочники
    Route::get('/get-guides', [GetGuidesController::class,'guides'] ); // справочник категорий, типов, тэгов
    Route::get('/get-relatives', [GetGuidesController::class,'relatives'] ); // связи между тэгами и статьями

    // ресурс со статьями
    Route::resource('/articles', ArticleMainController::class)
        ->only(['index', 'show', 'update', 'create', 'store']); // ресурс имеет CRUD методы для работы с публикациями

    // файловый менеджер
    Route::get('/upload-files-init/{id}', [UploadFilesController::class, 'initStorage']); // инициализация хранилища
    Route::post('/upload-files-add', [UploadFilesController::class, 'addFiles']); // добавление файлов к статье в хранилище
    Route::get('/upload-files-read/{id}', [UploadFilesController::class, 'readFiles']); // чтение структуры
    Route::get('/upload-files-get-link/{id}', [UploadFilesController::class, 'getLink']); // чтение структуры
    Route::post('/upload-files-mkDir', [UploadFilesController::class, 'mkDir']); // создание папки

    // получение статичных файлов
    Route::get('/download/{file}', DownloadsController::class);

    // загрузка/ получение обложек
    Route::post('/upload-cover',[CoverLoadController::class , 'uploadCoverImg']);




    // ресурс с тэгами
    Route::resource('/tags_articles', ArticleTagController::class)
        ->only(['update']); // ресурс имеет CRUD методы для работы с тэгами

    // данные для дашбоарда
    Route::get('/getArticlesStat', DashBoardController::class);

    // ресурс с комментариями
    Route::resource('/comments', CommentsController::class)->only(['store', 'index']); // ресурсный контроллер комментариев

    // запрос на смену статуса статьи
    Route::get('/changeStatus', ChangeArticleStatusController::class)->middleware('status.change');



});

