<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cross-Origin Resource Sharing (CORS) Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure your settings for cross-origin resource sharing
    | or "CORS". This determines what cross-origin operations may execute
    | in web browsers. You are free to adjust these settings as needed.
    |
    | To learn more: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
    |
    */

    'paths' => ['api/*', 'sanctum/csrf-cookie'],

    'allowed_methods' => ['*'],

    // доступ с фронта на хосте и с сервера разработчика
    'allowed_origins' => ['https://k-base.dglv.ru','http://localhost:3000'],

    'allowed_origins_patterns' => [],

    //  после авторизации все запросы сопровождается этим заголовком
    'allowed_headers' => ['Authorization', 'Content-Type'],

    'exposed_headers' => [],

    // preflight запрос кешируется на сутки, это ускоряет обмен данными с сервером
    'max_age' => 86400,

    'supports_credentials' => false,

];
