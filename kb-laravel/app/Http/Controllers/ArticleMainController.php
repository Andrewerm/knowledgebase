<?php

namespace App\Http\Controllers;

use App\Models\ArticleMain;
use App\Services\TagsOperations;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ArticleMainController extends Controller
{
    /**
     * @var ArticleMain
     */
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
//    public function __construct(Request $request)
//    {
//        dump($request->route()->getName());
//
//    }

    public function index(Request $request, ArticleMain $article)
    {
        $user=auth()->user();
        $userID=$user->id; // ID юзера
        $userRoleID=$user->role->id; // получаем роль пользователя
        if (!in_array($userRoleID, [1,3])) // если Админ или модератор
          { // иначе только опубликованные или все подряд в которых юзер это создатель
            $article=$article->where(function ($query) use ($userID) {  // условие объединили в блок (как в скобки)
                $query->where ('status_article',3)
                    ->orwhere('created_by_user', $userID); // получаем ссылку на модель
            });
        }
        $reqFields=$request['fields']; // получаем параметры GET запроса, в field требуемые поля
        $reqFilter=$request['filter']; // строка фильтра
        if (isset($reqFilter)){
            $article=$article
                ->where(function ($query) use ($reqFilter){  // условие объединили в блок (как в скобки)
                    $query->where('title_article', 'like','%'.$reqFilter.'%')
                        ->orwhere('content_article', 'like','%'.$reqFilter.'%')
                        ->orwhere('company', 'like','%'.$reqFilter.'%')
                        ->orwhere('description', 'like','%'.$reqFilter.'%')
                        ->orwhere('project_name', 'like','%'.$reqFilter.'%')
                        ->orwhere('work', 'like','%'.$reqFilter.'%')
                        ->orwhere('effect', 'like','%'.$reqFilter.'%')
                        ->orwhere('targets', 'like','%'.$reqFilter.'%')
                        ->orwhere('experience_consulting', 'like','%'.$reqFilter.'%')
                        ->orwhere('experience_school', 'like','%'.$reqFilter.'%')
                        ->orwhere('experience_laboratory', 'like','%'.$reqFilter.'%')
                        ->orwhere('reasons', 'like','%'.$reqFilter.'%')
                        ->orwhere('problem', 'like','%'.$reqFilter.'%')
                        ->orwhere('solution', 'like','%'.$reqFilter.'%')
                        ->orwhere('before', 'like','%'.$reqFilter.'%')
                        ->orwhere('after', 'like','%'.$reqFilter.'%')
                        ->orwhere('actions', 'like','%'.$reqFilter.'%');
                })
                ->get($reqFields);}
            else {
                $article=$article
                    ->get($reqFields);}
        $article=$article->map( function ( $item ){
            $item['coverUrl']=$this->getCoverImg($item['id']);
            return $item;
        });
        return response()->json(['rows'=>$article]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create()
    {
        // возвращает модель данных
        $consult=ArticleMain::select('id','title_article')->where('type_article',1)->orderBy('title_article')->get();
        return response()->json(array(
            "error"=>0,
            "info"=> ArticleMain::modelInfo(),
            "rows" => [],
            "consulting_articles"=>$consult
        ));
    }

    private function getCoverImg(int $idArticle): string
    {
        $idArticle=(string) $idArticle;
        $filePath="covers/".$idArticle.".jpg";
        if (Storage::disk('public')->exists($filePath)){
            $returnUrl= Storage::disk('public')->url($filePath);
        }
        else {
            $returnUrl=Storage::disk('public')->url("covers/placeholder.jpg");
        }
        return $returnUrl;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $newData=$request->all();
        $newData['created_by_user']=auth()->user()->id; //  добавляем ID юзера, создавшего статью
        if (array_key_exists('kpi',$newData )) $newData['kpi']=json_encode($newData['kpi']); // какой то глюк, без этого JSON не воспринимается. $casts в модели игнорируется
        // создание новой статьи, возвращает ID новой статьи
        $id=ArticleMain::insertGetId($newData);
        if ($newData['type_article']==2) TagsOperations::copyTags($newData['parent_article_id'],$id );
        return response()->json(array(
            "error"=> 0,
            "id"=>$id
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArticleMain  $articleMain
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        // выдаёт по ID все поля статьи
        $article=ArticleMain::find($id);
        if (is_null($article)) return response()->json(["error"=>"нет такой публикации"],404);
        if (isset($article)){$article->increment('counter');}; // увеличение счётчика (инкрементация) при каждом запросе
        $result=array(
            "error"=>isset($article)?0:1, // если такой статьи нет, то error=1
            "info"=> $article::modelInfo(),
            "rows" => array(isset($article)?$article: null ),
            "permissions"=>$article->permissionForChangeStatuses(),
            "authorInfo"=>$article->authorArticleInfo(),
            "coverUrl"=>$this->getCoverImg($id)  // получение обложки статьи
        );
        $parent=$article->getParent();
        $children=$article->getchildren();
        if ($parent) $result['parent']=$parent;
        if ($children) $result['children']=$children;
        return response()->json($result);
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArticleMain  $articleMain
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArticleMain  $articleMain
     * @return JsonResponse
     */
    public function update(Request $request, $idArticle)
    {  // изменения в статье
        $article=ArticleMain::find($idArticle); // получаем модель из $this->articles (определено в конструкторе)
        $input = $request->all(); // получаем все параметры из тела запроса
        $article->update($input); // массовое изменение параметров
        return response()->json(array(
            "error"=>isset($article)?0:1, // если такой статьи нет, то error=1
        ));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArticleMain  $articleMain
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticleMain $articleMain)
    {
        //
    }
}
