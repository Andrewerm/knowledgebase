<?php

namespace App\Http\Controllers;

use App\Services\FileLinksDirectoryService;
use App\Services\FilesLoader;
use App\Services\TransportYandexDisk;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UploadFilesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */

    public function initStorage(int $idArticle): JsonResponse
    {
        $cloud=new TransportYandexDisk(env('YD_API_KEY'));
        $result=['files_structure'=>FileLinksDirectoryService::initFileStorage($idArticle), 'freeSpace'=>$cloud->getFreeSpace()];
        return response()->json($result);
    }

    // получение временной ссылки на файл
    public function getLink(string $idContainer)
    {
        $cloud=new TransportYandexDisk(env('YD_API_KEY'));
        $link=$cloud->getTempLink($idContainer);
        $resultFormat=['tempLink'=>$link];
        return response()->json($resultFormat);
    }

    public function addFiles(Request $request): JsonResponse
    {
        $cloud=new TransportYandexDisk(env('YD_API_KEY'));
        $receivedFilesCollection = $request->collect('files'); // получаем из запроса список файлов
        $articleID = (int)$request->input('article'); // получаем id статьи и переводим в integer
        $articleLoadingPath=$request->input('path'); //  получаем путь куда загружать файлы
        $loaderResult=FilesLoader::loading($receivedFilesCollection); // загрузка файлов на сервер
        $setTaskResult=FilesLoader::setLoadingTasks($loaderResult); // проверка файлов на существующие и постановка задач на загрузку в облако
        $result=FileLinksDirectoryService::addFiles($setTaskResult, $articleLoadingPath, $articleID); // добавление списка файлов в таблицу связей статей с файлами
        $fullResult=$cloud->getFilesInfo($result); // обогащение данными из облака
        $resultFormat=['files_structure'=>$fullResult, 'freeSpace'=>$cloud->getFreeSpace()];
        return response()->json($resultFormat); // возвращаем обновлённую структуру

    }
    // чтение структуры файлов
    public function readFiles(int $idArticle): JsonResponse
    {
        $cloud=new TransportYandexDisk(env('YD_API_KEY'));
        $result=FileLinksDirectoryService::readFilesStorage($idArticle);
        $fullResult=$cloud->getFilesInfo($result); // обогащение данными из БД
        $resultFormat=['files_structure'=>$fullResult, 'freeSpace'=>$cloud->getFreeSpace()];
        return response()->json($resultFormat);
    }

    public function mkDir(Request $request): JsonResponse
    {
        $articleID = (int)$request->input('article'); // получаем id статьи и переводим в integer
        $articlePath=$request->input('path'); //  получаем путь где создавать папку
        $nameDir=$request->input('nameDir'); //  имя папки
        $result=FileLinksDirectoryService::mkDir($articleID, $articlePath, $nameDir);
        $resultFormat=['files_structure'=>$result];
        return response()->json($resultFormat); // возвращаем обновлённую структуру

    }
}
