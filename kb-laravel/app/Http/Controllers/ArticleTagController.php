<?php

namespace App\Http\Controllers;

use App\Models\ArticleTag;
use App\Models\ArticleTagLink;
use Illuminate\Http\Request;
use function Clue\StreamFilter\fun;

class ArticleTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idArticle)
    {  // обновления тэгов и ОКВЭДов статьи
        $id=(int) $idArticle; // преобразуем в integer
        $input=$request->collect(); // получаем все параметры из запроса
        ArticleTagLink::where('article_id', $id)->delete(); // удаляем все связи статьи
        $input->each(function ($value) use ($id) {
            $res=array("article_id"=>$id);
            if (is_string($value)) $res['okved']=$value;
               else $res['tags_id']=$value['id'];
            ArticleTagLink::create($res);}
        );
        return response()->json(["error"=>0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
