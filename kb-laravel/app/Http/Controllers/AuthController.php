<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use \App\Services\BX24Helper;

class AuthController extends Controller
{
    //
    public function login(Request $request){
        $request->validate([
            'login' => 'required',
            'password' => 'required'
        ]);

        if ($request->server == "local") {
           //local
            $user = User::where('login', $request->login)->first();
            if (! $user || ! Hash::check($request->password, $user->password)) {
                $data=['error'=>'Неверный логин или пароль'];
                return response()->json($data, 401);
            }
            $token = $user->createToken($request->login)->plainTextToken;
        } else {
           //bx24
            $bx24 = new BX24Helper();
            $token = $bx24->login($request->login, $request->password);
        }

        $data = ['user'=>[
            'token' => $token,
        ]];

        return response()->json($data, 200);
    }

    public function logout(){
        $user = auth()->user();
        foreach ($user->tokens as $token) {
            $token->delete();
        }

        return response()->json(
            ['error'=>0,
                'message'=>'Logged out successfully']
            , 200);
    }

    public function userInfo(){
        $user = auth()->user();
        return response()->json([
            'error'=>0,
            'user'=>[
                'fio'=>$user['fio'],
                'photo'=>$user['photo'],
                'position_name'=>$user['position_name'],
                'department_name'=>$user['department_name'],
                'role_name'=>$user['role']['name']
            ]
        ], 200);
    }
}
