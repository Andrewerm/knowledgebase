<?php

namespace App\Http\Controllers;

use App\Models\ArticleStatus;
use App\Models\ArticleTag;
use App\Models\ArticleTagLink;
use App\Models\ArticleTagsCategory;
use App\Models\ArticleType;

class GetGuidesController extends Controller
{ // контроллер справочника статусов, тэгов, типов и т.д.
    public function guides(ArticleStatus $articleStatus,
                             ArticleTag $articleTag,
                             ArticleType $articleType,
                            ArticleTagsCategory $articleTagsCategory){
        $statuses=$articleStatus->select('id', 'name_status', 'color_status')->get();
        $tags=$articleTag->get(['id', 'name_tag', 'category_id']);
        $types=$articleType->get(['id', 'name', 'color', 'icon']);
//        $types->map(function ($item){
////           lad($item);
////           $item['files_storage']=count($item['pattern_directories_structure'])>0;
//           unset($item['pattern_directories_structure']);
//           return $item;
//        });
        $categories=$articleTagsCategory->get(['id', 'name_category', 'caption_category', 'category_icon']);
        return response()
            ->json(array(
                'statuses'=>$statuses,
                'article_types'=>$types,
                'tags'=>$tags,
                'category_tags'=>$categories));

    }
    public function relatives(ArticleTagLink $articleTagLink)
    { // контроллер загрузки связей между статьями и тэгами
        $links=$articleTagLink->get(['tags_id', 'article_id', 'okved']);
        return response()->json(array('tags_articles'=>$links ));

    }
}
