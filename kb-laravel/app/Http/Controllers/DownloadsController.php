<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadsController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // позволяет скачать статичный файл из storage/app/public
    public function __invoke(Request $request, $file)
    {
        $filePath = storage_path('app/public/'.$file);
//        if (! file_exists($filePath)) {
//            // Some response with error message...
//        }
        return response()->download($filePath);
        //
    }
}
