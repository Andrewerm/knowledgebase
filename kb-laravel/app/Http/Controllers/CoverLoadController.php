<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CoverLoadController extends Controller
{
    //
    public function uploadCoverImg(Request $request): JsonResponse
    {
        $mode=$request->get('mode');
        $content=$request->get('content');
        $idArticle=(string) $request->get('id');
        if ($mode=='file'){
            $fileContent= Str::of($content)->after(','); // убираем служебную информацию до запятой
            Storage::disk('public')->put('covers/'.$idArticle.".jpg", base64_decode($fileContent));
            $url=Storage::disk('public')->url("covers/".$idArticle.".jpg");
            return response()->json([
                "error"=>0,
                "coverUrl"=>$url
            ]);
        }
        if ($mode=='link'){
            $path = Storage::disk('public')->path("covers/".$idArticle.".jpg");
            copy($content, $path); // получение файла из внешнего ресурса
            $coverUrl=Storage::disk('public')->url("covers/".$idArticle.".jpg");
            return response()->json(["error"=>0,
                "coverUrl"=>$coverUrl
                ]);
        }
        return response()->json(["error"], 403);


    }
}
