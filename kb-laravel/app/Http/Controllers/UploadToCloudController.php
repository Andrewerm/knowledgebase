<?php

namespace App\Http\Controllers;

use App\Models\FileUploadTask;
use App\Services\TransportYandexDisk;
use Illuminate\Http\JsonResponse;

class UploadToCloudController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function __invoke(): JsonResponse
    {
        $apikey=env('YD_API_KEY');
        $cloud=new TransportYandexDisk($apikey);
        $listFileToUpload=FileUploadTask::where('taskStatus','new')->get(); // получаем список необработанных файлов
        $res=$cloud->upload($listFileToUpload);
        return response()->json($res);

    }
}
