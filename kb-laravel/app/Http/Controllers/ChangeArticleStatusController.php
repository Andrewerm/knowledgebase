<?php

namespace App\Http\Controllers;

use App\Models\ArticleMain;
use Illuminate\Http\Request;

class ChangeArticleStatusController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        //
        $newStatus=$request->get('status');
        $articleID=$request->get('article_id');
//        $user=auth()->user();
        $article=ArticleMain::find($articleID);
        $article->status_article=$newStatus;
        $article->save();
        $updatedStatus=$article->status_article;
        return response()->json([
            'error'=>0,
            'updated_status'=>$updatedStatus
        ]);
    }
}
