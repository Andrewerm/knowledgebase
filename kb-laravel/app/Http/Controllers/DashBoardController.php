<?php

namespace App\Http\Controllers;

use App\Models\ArticleMain;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DashBoardController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    private function getCoverImg(int $idArticle): string
    {
        $idArticle=(string) $idArticle;
        $filePath="covers/".$idArticle.".jpg";
        if (Storage::disk('public')->exists($filePath)){
            $returnUrl= Storage::disk('public')->url($filePath);
        }
        else {
            $returnUrl=Storage::disk('public')->url("covers/placeholder.jpg");
        }
        return $returnUrl;
    }

    public function __invoke(Request $request)
    {
        /** Метод со статистикой для dashboard. **/
        // общее кол-во статей (неопубликованных тоже)
        $articles_count=ArticleMain::query()->count('id');
        // кол-во пользователей (админа, system и guest не берем)
        $users_count=User::query()->whereNotIn('login', ["admin", "system", "guest"])->count('id');
        // Группирует по статусам, типам статей и считает их количество
        $articles=ArticleMain::query()->selectRaw('type_article, status_article, count(1) as count')
            ->groupBy('type_article', 'status_article')
            -> get();
        // по каждому типу статьи выдаёт топ-юзера  status_article=3
        // TODO переделать SQL запрос на использование внешних ключей, желательно ORM (сейчас подзапросы)
        $top_users= DB::select(
            'SELECT knowledgebase_article_types.id, knowledgebase_article_types.name, knowledgebase_article_types.color, knowledgebase_article_types.icon, users.fio, users.photo->> "$[0].src" as photo,
        (SELECT created_by_user FROM knowledgebase_articles WHERE type_article=knowledgebase_article_types.id and status_article=3 GROUP BY created_by_user ORDER BY COUNT(created_by_user) DESC LIMIT 1) as user_id,
        (SELECT COUNT(created_by_user) FROM knowledgebase_articles WHERE type_article=knowledgebase_article_types.id and status_article=3 GROUP BY created_by_user ORDER BY COUNT(created_by_user) DESC LIMIT 1) as count
        FROM knowledgebase_article_types JOIN users ON users.id=(SELECT created_by_user FROM knowledgebase_articles
        WHERE type_article=knowledgebase_article_types.id and status_article=3 GROUP BY created_by_user ORDER BY COUNT(created_by_user) DESC LIMIT 1)
        WHERE (SELECT COUNT(created_by_user) FROM knowledgebase_articles
        WHERE type_article=knowledgebase_article_types.id and status_article=3 GROUP BY created_by_user ORDER BY COUNT(created_by_user) DESC LIMIT 1) >0');
        /* личный рейтинг пользователей  status_article=3
        rating (оценка публикации)- имеет вес 200
        count_articles (кол-во опубликованных статей пользователя) имеет вес 10
        files_added (кол-во добавлений файлов в чужие статьи ) пока нет и всем 0
        */
        $private_rating= DB::select('
        SELECT id, fio, position_name, photo->>"$[0].src" as photo,
       (select count(1) from knowledgebase_articles where status_article=3 and created_by_user=users.id) as count_articles,
       (select avg(rating) from knowledgebase_articles where created_by_user=users.id)  as rating_articles,
       0 as files_added, round(
       (select count(1) from knowledgebase_articles where status_article=3 and created_by_user=users.id)*10+
       (select avg(rating) from knowledgebase_articles where created_by_user=users.id)*200) as private_rating
        from users where login not in ("system", "admin", "login", "guest") order by private_rating  desc limit 3
        ');


        // Статистика по кол-ву созданных статей в месяц
        $createStatistic=ArticleMain::selectRaw('DATE_FORMAT(created_at, "%Y-%m") as date, count(1) as count')
            -> groupBy('date')
            -> orderBy('date')
            -> get();
        // Статистика по кол-ву опубликованных статей в месяц
        $updateStatistic=ArticleMain::selectRaw('DATE_FORMAT(updated_at, "%Y-%m") as date, count(1) as count')
            -> whereNotNull('updated_at') // фильтруем статьи, которые не имеют updated
            -> groupBy('date')
            -> orderBy('date')
            -> get();
        // Статистика по самым популярным статьям
        $mostPopularArticles=ArticleMain::where('counter', '>', 0)
            ->orderBy('counter', 'desc')
            ->take(4) // ограничение по количеству
            ->select('id', 'type_article', 'title_article')
            ->get();
        $mostPopularArticles=$mostPopularArticles->map( function ( $item ) {
            $item['coverUrl'] = $this->getCoverImg($item['id']);
            return $item;
        });

        return ["articles_counts"=>$articles,
            "top_users" => $top_users,
            "articles_count"=>$articles_count,
            "users_count" => $users_count,
            "created_statistic"=>$createStatistic,
            "updated_statistic"=>$updateStatistic,
            "most_popular_articles"=>$mostPopularArticles,
            "private_rating"=> $private_rating
        ];
        //
    }
}
