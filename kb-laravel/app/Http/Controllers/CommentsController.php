<?php

namespace App\Http\Controllers;

use App\Models\ArticleComment;
use App\Models\User;
use Illuminate\Http\Request;
use function Clue\StreamFilter\fun;

class CommentsController extends Controller
{

    private function getComments($idArticle, $comments){
        // список комментариев по данной статье
        $articleCommentsList=$comments::where('article_id', $idArticle)->select('created_by_user','text','created_at')->orderBy('created_at', 'desc')->get();
        // справочник пользователей
        $uniqueUsers=$comments::where('article_id', $idArticle)->select('created_by_user')->distinct()->get();
        $uniqueUsersResult=[];  // берем только fio и photo
        foreach ($uniqueUsers as $item) {
            if (is_null($item->user)) $result=['fio'=>'Неизвестный пользователь',
                'photo'=>null];
            else $result=['fio'=>$item->user->fio,
                'photo'=>$item->user->photo[0]['src']
            ];
            $uniqueUsersResult[$item->created_by_user]=$result;
        }
        return['rows'=>$articleCommentsList,
            'usersInfo'=>$uniqueUsersResult];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, ArticleComment $comments)
    {
        $idArticle=$request->get('article_id');
        return response()->json($this->getComments($idArticle, $comments),200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ArticleComment $comments)
    {
        //
        $params=$request->all(); // получаем параметры
        $params['created_by_user']= auth()->user()['id']; // добавляем ID юзера из авторизации
        $comments::insert($params); // добавляем комментарий
        // возвращаем все комментарии по данной статье
        return response()->json($this->getComments($request['article_id'], $comments),200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
