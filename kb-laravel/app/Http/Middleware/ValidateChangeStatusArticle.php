<?php

namespace App\Http\Middleware;

use App\Models\ArticleMain;
use Closure;
use Illuminate\Http\Request;

class ValidateChangeStatusArticle
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $newStatus=$request->get('status'); // получаем новый статус
        $idArticle=$request->get('article_id'); // ID статьи
        $article=ArticleMain::find($idArticle); // статья
        $rights=$article->rightsOfCurrentUser(); // получаем права пользователя на данную статью
        if ((in_array($newStatus,$rights) || in_array('*',$rights)) && $article->status_article!=$newStatus) return $next($request);
        return response()->json(
            ['error'=>1,
            'message'=>'Нет прав на изменение статуса публикации']
        );
    }
}
