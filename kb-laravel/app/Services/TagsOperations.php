<?php

namespace App\Services;

use App\Models\ArticleTagLink;

class TagsOperations
{
    // статический метод копирует все тэги и ОКВЭды из одной статьи в другую
    public static function copyTags($source, $destination){
        $tags=ArticleTagLink::select('tags_id', 'okved')->where('article_id', $source)->get();
        $tags=$tags->map(function ($item) use ($destination) {
            $item['article_id']=$destination;
            return $item;
        });
        ArticleTagLink::insert($tags->toArray());
    }
}
