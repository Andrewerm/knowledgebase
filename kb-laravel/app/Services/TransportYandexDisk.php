<?php

namespace App\Services;

use App\Models\ArticleFileLink;
use App\Models\FileUploadTask;
use Arhitector\Yandex\Disk;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class TransportYandexDisk extends TransportBase
{
    public function __construct($apikey)
    {
        $this->disk = new Disk($apikey);
        $this->resource = $this->disk->getResource('app:/');
        $this->localStorage=Storage::disk('tempUploadsDisk');
    }

    // свободное место на диске
    public function getFreeSpace(){
        return round($this->disk['free_space']/1024/1024);
    }


    public function upload ($list){
//        $directories = Storage::disk('tempUploadsDisk')->directories('');
//        var_dump($directories);
        $result=collect();
        $list->each(function ($value) use ($result){
//            var_dump($value);
//            echo nl2br('этап 1'.PHP_EOL);
            $idContainer = $value->containerName;
//            echo nl2br('этап 2'.PHP_EOL);
            $filenameArray = $this->localStorage->files($idContainer);
//            echo nl2br('этап 3'.PHP_EOL);
            if (count($filenameArray)){
                // путь до файла
    //            var_dump($filename);
                $filename=$filenameArray[0];
//                echo nl2br('этап 4'.PHP_EOL);
    //            echo nl2br('filename '.$filename.PHP_EOL);
                $path = $this->localStorage->path($filename); // абсолютный путь до файла на сервере
//                echo nl2br('этап 5'.PHP_EOL);
    //            echo nl2br('path ' . $path.PHP_EOL);
                $folder = $this->disk->getResource('app:/' . $idContainer); // создание папки
                try {
//                    echo nl2br('этап 6'.PHP_EOL);
                    $folder->create(); // создание папки
                }catch (Exception $exception){

                }
                try {
//                    echo nl2br('этап 7'.PHP_EOL);
                    $resource = $this->disk->getResource('app:/' . $filename);
//                    echo nl2br('этап 8'.PHP_EOL);
                    $resource->upload($path); // загрузка в облако
//                    echo nl2br('этап 9'.PHP_EOL);
                    $value->taskStatus = 'loaded'; // меняем статус файла в таблице задач
                    $value->fileSize=$resource->get('size'); // получение размера файла
//                    echo nl2br('этап 10'.PHP_EOL);
                    $value->save(); // меняем статус файла в таблице задач
//                    echo nl2br('этап 11'.PHP_EOL);
//                    $this->localStorage->delete($filename); // удаляем файл
//                    echo nl2br('этап 12'.PHP_EOL);
                    $status='ok';
                }
                catch (Exception $exception){
                    $status=$exception->getMessage();
                }
                    $result->push([
                    'filename' => $filename,
                    'status'=>$status
                ]);
        }
            else {
                $value->delete(); // если файла не оказалось, то удаляем задачу
                $result->push([
                    'filename' => $idContainer,
                    'status'=>'absent'
                ]);
            };
            echo nl2br('этап 13'.PHP_EOL);
            try {
                $this->localStorage->deleteDirectory($idContainer); // удаляем контейнер
            }
            catch (Exception $exception){
                echo $exception->getMessage();
            }

        });
        return $result;
    }

    // обогащение файловой структуры данными из ЯД
    public function getFilesInfo(Collection $item): Collection
    {
        $res=$item->map(function ($elem) {
            if ($elem && $elem['type']=='file') { // если объект файл
                $data=$this->checkFileYdUploaded($elem['idContainer']);
                if ($data){ // проверка - загружен ли на ЯД
//                    $ydInfo=$this->infoYdFileInfo($elem['idContainer']); // запрашиваем данные с ЯД
                    return array_merge($elem, $data->toArray());} // объединяем данные
                else return $elem;  // если не загружен, то возвращаем как есть
            }
            else if ($elem && $elem['type']=='dir'){ // если объект папка
                $elem['content']=$this->getFilesInfo(collect($elem['content'])); // рекурсивный вызов той же функции
                return $elem;
            }
        });
        return $res;
    }

    // проверят загружен ли файл в облако
    private function checkFileYdUploaded(string $idContainer){
        return FileUploadTask::select('filePreview', 'fileSize')->where('containerName', $idContainer)->where('taskStatus','!=','new')->first();
        }

//
//    // по ID контейнера выдаёт из ЯД объект со всеми требуемыми полями
//    private function infoYdFileInfo(string $idContainer):array {
//        $resource=$this->getYdFileResource($idContainer); // получаем ссылку на ресурс-файл
//        $info=$resource->toArray(['name', 'type', 'size', 'preview']);
//        $info['tempLink']=$resource->getLink();
//        return $info;
//    }

    public function getTempLink(string $idContainer){
        $resource=$this->getYdFileResource($idContainer); // получаем ссылку на ресурс-файл
        return $resource->getLink();
    }

    // возвращает ссылку на ресурс файла на ЯД, если нет такого, то ничего не возвращает
    private function getYdFileResource(string $idContainer){
        $resourceFolder=$this->disk->getResource('app:/'.$idContainer);
        return $resourceFolder->items->getFirst();
}



}
