<?php

namespace App\Services;

use App\Models\FileUploadTask;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FilesLoader
{
    public static function loading($filesCollection){
        // перебираем коллекцию с файлами и сохраняем на диск в папках со случайными именами
        $result=collect();
        $filesCollection->each(function ($value) use ($result) {
            $idContainer = Str::random(); // уникальная строка
            $fileContent= Str::of($value['file'])->after(','); // убираем служебную информацию до запятой
            Storage::disk('tempUploadsDisk')->put($idContainer.'/'.$value['name'], base64_decode($fileContent));
            unset($value['file']); // контент файла уже не нужно, мы его сохранили на диск
            $value['idContainer']=$idContainer;
            $value['type']='file'; // тип - файл
            $name=$value['name']; // вытаскиваем имя файла
            unset($value['name']); // удаляем имя файла из объекта
            $result->put($name, $value); // имя файла

        });
        return $result;

    }
    public static function setLoadingTasks($filesCollection){
        return $filesCollection->map(
            function ($value){
                $uploadedFile=FileUploadTask::where('fileHash',$value['hash'])->first();
                if (isset($uploadedFile)){
                    self::deleteFile($value['idContainer']); // удаляем контейнер, т.к. там повторный файл
                    $value['idContainer']=$uploadedFile->containerName; // подменяем ID контейнера
                    $uploadedFile->increment('countLinks'); // инкрементируем счётчик ссылок на данный файл
                }
                else{ // если такого файла ещё не было, то добавляем запись в таблицу задач
                    FileUploadTask::insert(
                        ['containerName'=>$value['idContainer'],
                        'fileHash'=>$value['hash']]
                    );
                }
                return $value;
            }
        );
    }
    // метод удаления контейнеров
    public static function deleteFile($idContainer){
        Storage::disk('tempUploadsDisk')->deleteDirectory($idContainer);
    }


}
