<?php
namespace App\Services;

use \App\Models\User;
use \App\Services\BX24Auth;
use \Laravel\Sanctum\PersonalAccessToken;


class BX24Helper
{

    public $access_token;
    public $refresh_token;
    public $domain;


    public function login($login, $password) {
        $this->authorize($login, $password);

        $bx24_user = $this->getCurrentUser();
        if (!isset($bx24_user["ID"]) || (int)$bx24_user["ID"]==0) throw new \Exception("bitrix get user error");

        $user = User::find($bx24_user["ID"]);
        if (!$user) {
           $user = new User();
           $user->id = $bx24_user["ID"];
           $user->role_id = 5;
           $user->status = 1;
        }
        $user  = $this->updateLocalUser($user, $bx24_user, $login, $password);
        $token = $user->createToken($login)->plainTextToken;
        $pat = PersonalAccessToken::find(explode("|",$token)[0]);
        $pat->token = hash('sha256', $user->token);
        $pat->save();
        $token = $user->token;

        return $token;
    }


    public function updateLocalUser($user, $bx24_user, $login, $password) {
        $department = $this->getDepartment( $bx24_user["UF_DEPARTMENT"][0] )[0];
        $user->login = $login;
        $user->password = password_hash($password, PASSWORD_DEFAULT);
        $user->fio = $bx24_user["LAST_NAME"]." ".$bx24_user["NAME"]." ".$bx24_user["SECOND_NAME"];
        $user->email = $bx24_user["EMAIL"];
        if (strlen($bx24_user["PERSONAL_BIRTHDAY"])>9) $user->born = substr($bx24_user["PERSONAL_BIRTHDAY"], 0, 10);
        $user->department_id = $department["ID"];
        $user->department_name = $department["NAME"];
        $user->position_name = $bx24_user["WORK_POSITION"];
        $user->phone = $bx24_user["PERSONAL_MOBILE"];
        $user->photo = [ ["type"=>2, "name"=>"photo.jpg", "src"=>$bx24_user["PERSONAL_PHOTO"]] ];
        $user->token = $this->access_token;
        $user->token_expire = date("Y-m-d H:i:s", strtotime("+1 hours"));
        $user->refresh_token = $this->refresh_token;
        $user->refresh_token_expire = date("Y-m-d H:i:s", strtotime("+96 hours"));
        $user->domain = $this->domain;
        $user->save();
        return $user;
    }


    public function authorize($login, $password) {
        $bx24auth = new BX24Auth();
        $bx24auth->setApplicationId('local.5ee11901cdceb1.19720500');
        $bx24auth->setApplicationSecret('143q2yb9k1Mura1Q50517JUHaf3lbUcQAGPtbb1mfzvJBQct4z');
        $bx24auth->setApplicationScope('department,user,task');
        $bx24auth->setBitrix24Domain('leanvector.bitrix24.ru');
        $bx24auth->setBitrix24Login($login);
        $bx24auth->setBitrix24Password($password);
        try {
            $bx24auth->initialize();
        } catch (\Exception $error) {
            throw new \Exception($error->getMessage());
        }
        if (!isset($bx24auth->bitrix24_access["access_token"]) || strlen($bx24auth->bitrix24_access["access_token"])<9) throw new \Exception("bitrix login error");

        $this->access_token  = $bx24auth->bitrix24_access["access_token"];
        $this->refresh_token = $bx24auth->bitrix24_access["refresh_token"];
        $this->domain = $bx24auth->bitrix24_access["domain"];
    }


    public function getCurrentUser() {
        $url = "https://".$this->domain."/rest/user.current.json?auth=".$this->access_token;
        $content = file_get_contents($url);
        $result = json_decode($content, true);
        return $result["result"];
    }


    public function getDepartment($id) {
        $url = "https://".$this->domain."/rest/";
        if ($id>0) {$id="&ID=".$id;} else {$id="";}
        $method = $url."department.get.json?auth=".$this->access_token.$id;
        $content = file_get_contents($method);
        $result = json_decode($content, true);
        return $result["result"];
    }


}//class
