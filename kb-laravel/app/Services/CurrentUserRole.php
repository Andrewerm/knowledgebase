<?php

namespace App\Services;

class CurrentUserRole
{
    // текущий пользователь - модератор
    public static function isModerator(): bool
    {
        return auth()->user()->role->id==3;
    }    // текущий пользователь - админ

    public static function isAdmin(): bool
    {
        return auth()->user()->role->id==1;
    }

}
