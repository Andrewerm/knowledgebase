<?php

namespace App\Services;

use App\Models\ArticleFileLink;
use App\Models\ArticleMain;
use Exception;
use Illuminate\Support\Str;

class FileLinksDirectoryService
{
    const NEWDIRECTORY=[
        "type"=>"dir",
        "content"=>
            ["123456789123456789123456789"=>false] // небольшой костыль, без подобного объекта сериализация делается в массив, а нужно объект
    ];

    // инициализация хранилища файлов для публикации путём создания записи в таблице с шаблоном структуры папок
    public static function initFileStorage(int $idArticle){
        if (!ArticleFileLink::where('article_id', $idArticle)->exists()){
            try {
                $patternDirectory = ArticleMain::find($idArticle)->type->pattern_directories_structure; // получаем экземпляр типа статьи через связь многие-к-одному
                if (is_null($patternDirectory)) return false; // для данного типа статьи нет шаблона
            } catch (Exception $ex) {return false;};
            //Создаём начальный шаблон для файловой структуры статьи. Номерной массив становится ассоциативным
            $createPattern = $patternDirectory->mapWithKeys(function ($item) {
                return [$item =>self::NEWDIRECTORY];
            });
            // создаём новую запись
            $result=new ArticleFileLink();
            $result->article_id=$idArticle;
            $result->files_structure=$createPattern;
            $result->save();
            return $result->files_structure;
        }
        else{ // если хранилище уже было инициализировано, то false
            return false;
        }
    }

    // чтение структуры файлов
    public static function readFilesStorage(int $idArticle){
        $structure=ArticleFileLink::find($idArticle);
            if (isset($structure)){
                return $structure->files_structure;
            }
            else return collect([]);
    }

    public static function addFiles($list,string $path, int $idArticle){
        $convertedPath=self::convertPath($path);
        // если записи по данной статье ещё не было, то создаём новую с пустым объектом
        $list->each(function ($value, $fileName) use ($convertedPath, $idArticle) {
            $fullPath=$convertedPath.'->'.$fileName;
            ArticleFileLink::where('article_id', $idArticle)->update([$fullPath=>$value]);
        });
        return self::readFilesStorage($idArticle); // возвращаем обновлённые данные
    }

    // из строкового пути делает строку для работы с Eloqent json методом
    private static function convertPath(string $path): string
    {
        $trim=Str::of($path)->ltrim('/'); // убираем первый слэш
        $result=Str::replace('/', '->content->', $trim);
        return 'files_structure->'.$result.'->content';

    }

    // создание папки
    public static function mkDir(int $idArticle, string $path, string $nameDir){
        $fullPath=self::convertPath($path).'->'.$nameDir;
        ArticleFileLink::where('article_id', $idArticle)->update([$fullPath=>self::NEWDIRECTORY]);
        return self::readFilesStorage($idArticle); // возвращаем обновлённые данные
    }

}
