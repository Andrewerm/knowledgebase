<?php

namespace App\Models;

use App\Services\CurrentUserRole;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ArticleMain extends Model
{
    use HasFactory;
    public $incrementing = false; // MySQL инкрементирует само
    protected $table='knowledgebase_articles';
    public $timestamps = false; // если нужно, чтобы created_at и update_at автоматом не ставили текущее время

    protected $guarded = array('id'); // от массового изменения защищён только ID, остальное разрешено менять массово
    protected $casts = [
        'kpi'=>'json'
    ];

    public function type(): BelongsTo // возвращает тип статьи через связь многие-к-одному
    {
        return $this->belongsTo(ArticleType::class, 'type_article');
    }

    // является ли текущий пользователь автором данной статьи
    private function isAuthorOfCurrentArticle(): bool
    {
        return $this->created_by_user==auth()->user()->id;
    }

    // возвращает права текущего пользователя по текущей статье
    public function rightsOfCurrentUser(){
        if ($this->isAuthorOfCurrentArticle()) $result=auth()->user()->role->rights['change_article_states_own'];
            else $result=auth()->user()->role->rights['change_article_states_any'];
        return $result;

    }
    // может ли редактировать текущий юзер текущую статью
    private function canEdit(): bool
    {
        // если админ или модератор, то редактировать публикации можно в любом состоянии
        if (CurrentUserRole::isAdmin() || CurrentUserRole::isModerator()) return true;
        // если автор статьи, то может редактировать когда черновик, архив, отклонено
        if ($this->isAuthorOfCurrentArticle() && in_array($this->status_article,[1, 4, 5])) return true;
        return false;
    }

    // возвращает информацию о доступных для данного юзер действий со статьёй. Используется для включения/выключения кнопок
    public function permissionForChangeStatuses(){
        // TODO добавить валидацию заполненности полей и тэгов
        $rights=$this->rightsOfCurrentUser();
        $result=['canEdit'=>$this->canEdit()];
        $statuses=['toModeration'=>2,
            'toPublication'=>3,
            'toDraft'=>1,
            'toDecline'=>4,
            'toArchive'=>5
            ];
        foreach ($statuses as $type=>$status){
            $result[$type]=(in_array($status,$rights) || in_array('*',$rights)) && $this->status_article!=$status;
        }
        return $result;
    }

    public function getParent(){ // получение заголовка родительской статьи
        $title=ArticleMain::where('id', $this->parent_article_id)->value('title_article');
        return $title?['title_article'=>$title,
            'id'=>$this->parent_article_id]:null;
    }

    public function getChildren(){ // получение заголовков детей статьи
        $children=ArticleMain::where('parent_article_id', $this->id)->select('id','title_article')->get();
        return $children->count()?$children:null;
    }

    // Статический метод, информация о структуре статьи, используется для создания новой статьи и для определения типов полей
    public static function modelInfo() {
        return [
//            "table"=>"knowledgebase_articles",
//            "primary_key"=>"id",
//            "category"=>"Система",
//            "name"=>"Статьи",
//            "sortBy"=>["id", ],
//            "itemsPerPage"=>100,
//            "itemsPerPageVariants"=>[50,100,200,300,500,1000,],
//            "read"=>[1,2,3,4,5,6,],
//            "add"=>[1,2,3,4,5,],
//            "edit"=>[1,2,3,4,5,],
//            "delete"=>[],
//            "filter"=>[
//                "created_at"=>["label"=>"Дата создания", "filterType"=>"like", "name"=>"created_at", ],
//                "created_by_user"=>["label"=>"Кто создал", "filterType"=>"=", "name"=>"created_by_user", ],
//            ],
            "columns"=>[
                "id"=>["type"=>"integer", "label"=>"id статьи", "read"=>[1,2,3,4,5,6,], "add"=>[], "edit"=>[], "name"=>"id", ],
                "created_at"=>["type"=>"timestamp", "label"=>"Дата создания", "hidden"=>true, "read"=>[1,2,3,4,5,6,], "add"=>[], "edit"=>[], "name"=>"created_at", ],
                "updated_at"=>["type"=>"timestamp", "label"=>"Дата изменения", "hidden"=>true, "read"=>[1,2,3,4,5,], "add"=>[], "edit"=>[], "name"=>"updated_at", ],
                "created_by_user"=>["type"=>"linkTable", "label"=>"Создано пользователем", "table"=>"users", "field"=>"login", "hidden"=>true, "read"=>[1,2,3,4,5,], "add"=>[], "edit"=>[], "name"=>"created_by_user", ],
                "product_id"=>["type"=>"linkTable", "label"=>"Тип продукта (не исп.)", "name"=>"product_id", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], "table"=>"knowledgebase_products", "field"=>"name_product", ],
                "title_article"=>["type"=>"string", "label"=>"Заголовок", "name"=>"title_article", "read"=>[1,2,3,4,5,6,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "status_article"=>["type"=>"linkTable", "label"=>"Статус публикации", "name"=>"status_article", "read"=>[1,2,3,4,5,6,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], "table"=>"knowledgebase_statuses", "field"=>"name_status", "object"=>true, ],
                "type_article"=>["type"=>"linkTable", "label"=>"Тип публикации", "name"=>"type_article", "read"=>[1,2,3,4,5,6,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], "table"=>"knowledgebase_article_types", "field"=>"[name]", "object"=>true, ],
                "cover_img"=>["type"=>"string", "label"=>"Адрес картинки обложки", "name"=>"cover_img", "read"=>[1,2,3,4,5,6,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "content_article"=>["type"=>"text", "label"=>"Текст статьи", "name"=>"content_article", "hidden"=>true, "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "parent_article_id"=>["type"=>"linkParent", "label"=>"Родительская публикация", "name"=>"parent_article_id", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "company"=>["type"=>"string", "label"=>"Наименование компании", "name"=>"company", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "start_date"=>["type"=>"date", "label"=>"Дата начала", "name"=>"start_date", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "end_date"=>["type"=>"date", "label"=>"Дата завершения", "name"=>"end_date", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "description"=>["type"=>"html", "label"=>"Предпосылки проекта", "name"=>"description", "hidden"=>true, "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "project_manager"=>["type"=>"string", "label"=>"Руководитель проекта", "name"=>"project_manager", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "project_members"=>["type"=>"string", "label"=>"Участники проекта", "name"=>"project_members", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "customer_manager"=>["type"=>"string", "label"=>"Руководитель от закачика", "name"=>"customer_manager", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "customer_members"=>["type"=>"string", "label"=>"Участники от заказчика", "name"=>"customer_members", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "targets"=>["type"=>"html", "label"=>"Цели и задачи", "name"=>"targets", "hidden"=>true, "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "work"=>["type"=>"html", "label"=>"Объем работ", "name"=>"work", "hidden"=>true, "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "effect"=>["type"=>"html", "label"=>"Направление эффекта", "name"=>"effect", "hidden"=>true, "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "kpi"=>["type"=>"json", "label"=>"Показатели проекта", "name"=>"kpi", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "publ_id"=>["type"=>"integer", "label"=>"Старый ID публикации", "name"=>"publ_id", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "experience_consulting"=>["type"=>"text", "label"=>"Извлеченные уроки: возможности использования нового опыта в направлении «Консалтинг»", "name"=>"experience_consulting", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "experience_school"=>["type"=>"text", "label"=>"Извлеченные уроки: возможности использования нового опыта в направлении «Обучения»", "name"=>"experience_school", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "experience_laboratory"=>["type"=>"text", "label"=>"Извлеченные уроки: возможности использования нового опыта в направлении «Лин лаборатории и игры»", "name"=>"experience_laboratory", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "experience_digital"=>["type"=>"text", "label"=>"Извлеченные уроки: возможности использования нового опыта в направлении «Цифровые продукты»", "name"=>"experience_digital", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "reasons"=>["type"=>"html", "label"=>"Причины возникновения", "name"=>"reasons", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "problem"=>["type"=>"html", "label"=>"Проблема", "name"=>"problem", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "solution"=>["type"=>"html", "label"=>"Решение", "name"=>"solution", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "before"=>["type"=>"html", "label"=>"До реализации", "name"=>"before", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "after"=>["type"=>"html", "label"=>"После реализации", "name"=>"after", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "actions"=>["type"=>"html", "label"=>"Что сделано", "name"=>"actions", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "add_type"=>["type"=>"integer", "label"=>"Другой тип", "name"=>"add_type", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "rating"=>["type"=>"double", "label"=>"Рейтинг", "name"=>"rating", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "author"=>["type"=>"string", "label"=>"Автор", "name"=>"author", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "count"=>["type"=>"string", "label"=>"Количество участников", "name"=>"count", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "duration"=>["type"=>"string", "label"=>"Продолжительность курса", "name"=>"duration", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "tags1"=>["type"=>"string", "label"=>"Тэги 1 (не исп.)", "name"=>"tags1", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "tags2"=>["type"=>"string", "label"=>"Тэги 2 (не исп.)", "name"=>"tags2", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "tags3"=>["type"=>"string", "label"=>"Тэги 3 (не исп.)", "name"=>"tags3", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "redactors"=>["type"=>"string", "label"=>"Редактировал", "name"=>"redactors", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "project_name"=>["type"=>"string", "label"=>"Краткое наименование проекта", "name"=>"project_name", "read"=>[1,2,3,4,5,], "add"=>[1,2,3,4,5,], "edit"=>[1,2,3,4,5,], ],
                "counter"=>["type"=>"integer", "label"=>"Количество просмотров", "name"=>"counter", "read"=>[1,2,3,4,5,6,], "add"=>[1,2,3,4,5,6,], "edit"=>[1,2,3,4,5,6,], "default"=>"0", "clearable"=>false, ],
            ],
//            "seeds"=>[],
        ];
}

    public function authorArticleInfo(){
        $author=User::find($this->created_by_user); // экземпляр пользователя-автора статьи
        if ($author)
        return [
        'fio'=>$author->fio,
        'photo'=>$author->photo];
    }
}

