<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleTagLink extends Model
{
    use HasFactory;
    public $incrementing = false; // MySQL инкрементирует само
    protected $table='knowledgebase_tags_articles';
    protected $guarded = array('id'); // от массового изменения защищён только ID, остальное разрешено менять массово
//    public $timestamps = false; // если нужно, чтобы created_at и update_at автоматом не ставили текущее время
}
