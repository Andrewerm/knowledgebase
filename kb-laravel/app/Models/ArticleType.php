<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleType extends Model
{
    use HasFactory;
    public $incrementing = false; // MySQL инкрементирует само
    protected $table = 'knowledgebase_article_types';
    protected $casts = [ 'pattern_directories_structure' => 'collection']; // авто преобразование json поля в коллекцию

}
