<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ArticleComment extends Model
{
    use HasFactory;
    public $incrementing = false; // MySQL инкрементирует само
    protected $table = 'knowledgebase_comments';
    public $timestamps = false; // если нужно, чтобы created_at и update_at автоматом не ставили текущее время
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class,'created_by_user'); // возвращает экземпляр User
    }

}
