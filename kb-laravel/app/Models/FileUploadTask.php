<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileUploadTask extends Model
{ // таблица с задачами на загрузку файлов с сервера в облачное хранилище
    use HasFactory;
    protected $table='knowledgebase_uploaded_files';
    protected $primaryKey='containerName';
    public $incrementing = false; // потому что primaryKey это char тип
}
