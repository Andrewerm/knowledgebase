<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleFileLink extends Model
{  // таблица связана с knowledgebase_article один к одному. В ней хранится структура привязанных к статье файлов
    use HasFactory;
    protected $table='knowledgebase_article_links_files';
    protected $primaryKey='article_id'; // первичный ключ отличается от соглашения (id), поэтому указываем
    public $incrementing = false; // автоинкрементация не нужна
    public $timestamps = false; // created_at and updated_at не используются
    protected $casts = [ // авто преобразование json поля в массив
        'files_structure'  => 'collection' // авто преобразование json поля в коллекцию
    ];

    // пока не требуется
//    public function article(): HasOne
//    {
//        return $this->hasOne(ArticleMain::class, 'article_id');
//    }
}
