# API Базы знаний (KnowledgeBase)

##  1. Метод getAllArticles

### URL
/knowledgebase/api/v1/getAllArticles

### Тип запроса
POST

### Параметры запроса

|  Параметр  |  Тип   |  Описание  |   Обязателен | Пример | 
|--------------| :--------: |---------------| --- | ---
| page_size | number | Число страниц, которые нужно вывести| Да | 20
| current_page| number |Текущая страница (по умолчанию 1) | Нет | 3
| filter_params | string | Параметры фильтра через запятую|  нет | строитель, врач, учитель


### Пример запроса
``` json
{
  "page_size": 20,
  "current_page": 2,
  "filter_params": "строитель, врач, учитель",
}
```
### Пример ответа

``` json
{
  "articles": [
    {
      "id_article": 464,
      "created_article": "2021-09-03 15:41:54",
      "created_by_user_article": 5,
      "title_article": '«Бережливое производство + шесть сигм» в сфере услуг: '
      "product_id": 8,
      "status_article": 1,
      "type_article": 3,
      "cover_img": '../images/3.jpg'
    },
    }
```


##  2. Метод getArticle

### URL
/knowledgebase/api/v1/getArticle

### Тип запроса
POST

### Параметры запроса

|  Параметр  |  Тип   |  Описание  |   Обязателен | Пример | 
|--------------| :--------: |---------------| --- | ---
| id_article | number | Id статьи | Да | 5

### Пример запроса
``` json
{
  "id_article": 5,
}
```
### Пример ответа

``` json
{
      "id_article": 464,
      "created_article": "2021-09-03 15:41:54",
      "created_by_user_article": "Иванов Сергей",
      "parent_id": 400 // ссылка на родительскую статью если есть,
      "child_articles": [{
                   "child_article_id": 456,
                   "child_article_title": "Заголовок дочерней статьи"}
                   ], // ссылки на связанные статьи
      "product_id": 8,
      "title_article": '«Бережливое производство + шесть сигм» в сфере услуг: '
      "status_article": 1,
      "type_article": 3,
      "cover_img": '../images/3.jpg',
      "content_article": 'Много много текста с HTML разметкой'
    }
```

## 3. Метод addArticle

### URL
/knowledgebase/api/v1/addArticle

### Тип запроса
POST

### Параметры запроса
|  Параметр  |  Тип   |  Описание  |   Обязателен | Пример | 
|--------------| :--------: |---------------| --- | ---
| parent_id | number | ссылка на родительскую статью | нет | 465
| user_id | number | ссылка на пользователя - автора статьи | да | 12 
| product_id | number | ссылка на тип продукта | да | 10
| title_article | string | заголовок статьи | да | "«Бережливое производство + шесть сигм» в сфере услуг:"
| type_article | number | ссылка на тип статьи | да | 8
| cover_img | string | ссылка картинку обложку статьи | нет | '../images/3.jpg'
| content_article | string | основной текст стати | нет | "Много текста"

### Примечание
Статьи всегда добавляются в базу с status_article=1 (черновик)

### Пример ответа

``` json
{
      "id_article": 464, // если добавление статьи успешно
      "error": true, //  флаг ошибки добавления статьи
      "error_message" : // текст ошибки добавления статьи
    }
``` 

## 4. Метод deleteArticle

### URL
/knowledgebase/api/v1/deleteArticle


### Тип запроса
POST

### Параметры запроса

|  Параметр  |  Тип   |  Описание  |   Обязателен | Пример | 
|--------------| :--------: |---------------| --- | ---
| id | number | Id статьи | Да | 5

### Пример ответа

``` json
{
      "error": true, // флаг ошибки удаления статьи
      "error_message" : // текст ошибки удаления статьи
    }
``` 

## 5. Метод updateArticle

### URL
/knowledgebase/api/v1/updateArticle

### Тип запроса
POST

### Параметры запроса
|  Параметр  |  Тип   |  Описание  |   Обязателен | Пример | 
|--------------| :--------: |---------------| --- | ---
| id_article | number | Id статьи | Да | 5
| user_id | number | ссылка на пользователя - автора статьи | да | 12
| product_id | number | ссылка на тип продукта | да | 10
| title_article | string | заголовок статьи | да | "«Бережливое производство + шесть сигм» в сфере услуг:"
| type_article | number | ссылка на тип статьи | да | 8
| cover_img | string | ссылка картинку обложку статьи | нет | '../images/3.jpg'
| content_article | string | основной текст стати | нет | "Много текста"

### Примечание
Статьи всегда обновляются в базе с status_article=1 (черновик)

### Пример ответа

``` json
{
      "error": true, // флаг ошибки обновления статьи
      "error_message" : "статья с таким ID отсутствует" // текст ошибки обновления статьи
    }
```

## 6. Метод getGuides
предназначен для загрузки справочников при запуске WEB приложения
### URL
/knowledgebase/api/v1/getGuides

### Тип запроса
POST

### Параметры запроса
без параметров

### Пример ответа

``` json
{
     "users": [{
     "id_user":18, "fullName_user": "Петров Иван"}],
     "products": [{"id_product": 12, 
     "name_product": "Обучение"}],
     "article_types": [{
     "id_article_type": 18,
     "article_type_name": "Литература"
     }]
    }
``` 


## 7. Метод updateGuideTypesArticles

## 8. Метод updateGuideProducts

## 9. Метод addComment

## 10. getCommentsByArticleID

