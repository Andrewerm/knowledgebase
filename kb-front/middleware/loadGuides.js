export default async function ({ $auth, store, $toast }) { // загрузка справочников
  if ($auth.loggedIn && !store.state.guides.guideLoaded) { // если авторизованы и если они не загружены ещё
    try {
    // загрузка справочника в store
      await store.dispatch('guides/set_guide')
      // eslint-disable-next-line no-console
      console.info('справочник загрузился')
      await store.dispatch('relatives/set_relatives')
      // eslint-disable-next-line no-console
      console.info('связи тэгов и ОКВЭД загрузились')
      await store.dispatch('guides/set_okved')
      console.info('справочник ОКВЭД загрузился')
    } catch
    (err) {
      $toast.error(`Проблемы ${err}`, { duration: 5000 })
    // await $auth.loginWith('local')
    }
  } else {
    console.log('loadGuides пропустили загрузку справочников')
  }
}
