// TODO какая то проблем при загрузке публикаций напрямую, ошибка
export default async function (context) {
  if (context.$auth.loggedIn && context.$auth.strategy.token.status().valid()) { // если залогинен и ключ валидный, то выход
    console.log('1 preAuth loggedIN', context)
    return
  }
  console.log('context.$auth.loggedIn', context.$auth.loggedIn, 'context.$auth.strategy.token.status().valid()', context.$auth.strategy.token.status().valid())
  console.log('context.$auth.strategy.token', context.$auth.strategy.token)
  console.log('context.$auth', context.$auth, 'context.$auth.loggedIn', context.$auth.loggedIn)

  // пробуем получить токен из хранилища
  // const token = context.$auth.$storage.getLocalStorage('token')
  const token = context.$auth.strategy.token.get()
  console.log('3 preAuth token from store ', token)
  // TODO плавающая проблема с сохранением срока действия токена, он продляется на фронте иногда сам собой и не совпадает со сроком на сервере
  if (token && context.$auth.strategy.token.status().valid()) { // если токен есть в хранилище
    // context.$auth.strategy.token.set(token) // устанавливаем его
    // await context.$auth.login()
    context.$auth.$storage.setState('loggedIn', true) // ставим статус Залогинен
    console.log('8 preAuth токе взяли из хранилища context.$auth.loggedIn', context.$auth.loggedIn)
    const user = context.$auth.$storage.getLocalStorage('user') // берем из хранилища инфу о юзере
    context.$auth.setUser(user) // устанавливаем в нужное место
    return
  } else {
    console.log('preAuth токен не будем брать из store')
    context.$auth.$storage.removeLocalStorage('user')
    context.$auth.strategy.token.reset()
  }
  // eslint-disable-next-line no-undef
  // if (BX24) {
  // console.log('11. PreAuth авторизуемся  в Битриксе')
  // eslint-disable-next-line no-undef
  // const BXauth = BX24.getAuth()
  // const user = await new Promise(function (resolve) {
  //   // eslint-disable-next-line no-undef
  //   BX24.callMethod('user.current', {}, function (response) {
  //     resolve(response.answer.result)
  //   })
  // })
  // console.log('BX24 user', user)
  // try {
  //   const result = await context.$axios.post('/auth/me/bx24', { auth: BXauth, user })
  //   console.log('preAuth получили от Битрикса', result)
  //   const bUser = result.data.user
  //   context.$auth.strategy.token.set(bUser.token) // устанавливаем токен
  //   context.$auth.setUser(bUser) // устанавливаем юзера
  //   context.$auth.$storage.setLocalStorage('token', context.$auth.strategy.token.get())
  //   context.$auth.$storage.setLocalStorage('user', context.$auth.user)
  // } catch (e) {
  //   console.error('авторизация в Битрикс не прошла', e)
  // }
  //   return
  // }

  // если ничего не вышло, то пользователь направляется на страницу login
  console.log('10 Не смогли авторизоваться, идём в login', context.$auth.$state)
}
