import { OkvedObj } from 'assets/js/okved'

const state = () => ({
  tags: null,
  statuses: [],
  // products: null, не используем
  article_types: [],
  okved: [], // структурированный ОКВЭД
  okved_row: [], // "сырой" ОКВЭД
  category_tags: [], // Категории тэгов
  guideLoaded: false // флаг загрузки справочника
})

const getters = {
  // статус загрузки справочника
  guideLoaded: state => state.guideLoaded,
  // поиск Оквэда по индексу, возвращает объект со всеми данными
  getOkvedByID: state => (id) => {
    const result = state.okved_row.find(item => item.Kod === id)
    if (result) { return result } else { console.error(`${id} отстуствует в справочнике`) }
  },
  // поиск Оквэда по индексу, возвращает String
  getOkvedNameByID: state => (id) => {
    if (state.okved_row.length) { // пока Оквэд не загрузился показываем пустую строку
      const result = state.okved_row.find(item => item.Kod === id)
      if (result) { return `${result.Idx} - ${result.Name}` } else { console.error(`${id} отсутствует в справочнике`) }
    } else { return '' }
  },
  article_statuses: state => state.statuses.map(item => ({
    text: item.name_status,
    value: item.id,
    color: item.color_status
  })),
  // получаем по ID объект описывающий статус статьи
  getArticleStatusByID: state => (id) => {
    if (typeof (id) === 'string') id = parseInt(id)
    return state.statuses.find(item => item.id === id)
  },
  article_types: state => state.article_types.map((item) => {
    const result = {
      text: item.name,
      value: item.id,
      color: item.color,
      files_storage: item.files_storage
    }
    if (item.icon) { result.icon = 'mdi-' + item.icon } // mdi добавляем к имени
    return result
  }),
  // получаем по ID объект описывающий тип статьи
  getArticleTypeByID: state => (id) => {
    if (typeof (id) === 'string') id = parseInt(id)
    const res = Object.assign({}, state.article_types.find(item => item.id === id)) // копируем объект, чтобы можно было модифицировать icon
    if (res.icon) { res.icon = 'mdi-' + res.icon } // mdi добавляем к имени
    return res
  },
  // геттер специально для ПостФильтра
  getForSimpleListFilter: state => (field) => {
    if (field === 'type_article') {
      return state.article_types.map((item) => {
        const result = {
          text: item.name,
          value: item.id,
          color: item.color
        }
        if (item.icon) { result.icon = 'mdi-' + item.icon } // mdi добавляем к имени
        return result
      })
    }
    if (field === 'status_article') {
      return state.statuses.map(item => ({
        text: item.name_status,
        value: item.id,
        color: item.color_status
      }))
    }
  },
  // возвращает объект Тэг по его ID
  getTagByID: state => (link) => {
    // если в таблице связей данная запись имеет ОКВЭД, то возвращаем ОКВЭД, иначе тэг-объект
    if (link.okved) { return link.okved } else { return state.tags.find(item => item.id === link.tags_id) }
  },
  // возвращает объект- Категорию тэга по имени
  getCategoryByName: state => (name) => { return state.category_tags.find(item => item.name_category === name) },
  // по имени категории определяем её пользовательское имя
  getCaptionCategory: state => (name) => {
    return state.category_tags.find(item => item.name_category === name) || 'нет данных'
  },
  // по ID категории тэга возвращает её имя
  getNameCategoryByID: state => (id) => { return state.category_tags.find(item => item.id === id).name_category }
}

const mutations = {
  set_guide (state, guide) {
    state = Object.assign(state, guide, { guideLoaded: true })
  },
  set_okved (state, okved) {
    state.okved = okved
  },
  set_okved_row (state, okved) {
    state.okved_row = okved
  }
}

const actions = {
  async set_guide ({ commit }) { // загрузка справочника в store
    const result = await this.$axios.$get('/get-guides', {
    })
    commit('set_guide', result)
  },
  async set_okved ({ commit }) {
    const result = await this.$axios.$get('/download/okved.json')
    commit('set_okved_row', result)
    commit('set_okved', OkvedObj.call(result, result))
  }
}

export { state, getters, mutations, actions }
