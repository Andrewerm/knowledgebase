// здесь хранятся связи статей с тэгами и ОКВЭДами
const state = () => ({
  tags_articles: [],
  guideLoaded: false // флаг загрузки справочника
})

const getters = {
  // статус загрузки справочника
  guideLoaded: state => state.guideLoaded
}

const mutations = {
  set_relatives (state, relatives) {
    state = Object.assign(state, relatives, { guideLoaded: true }) // заодно проставляем статус загрузки данных
  }
}

const actions = {
  async set_relatives ({ commit }) {
    const result = await this.$axios.$get('/get-relatives')
    commit('set_relatives', result)
  }
}

export { state, getters, mutations, actions }
