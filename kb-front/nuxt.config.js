import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  auth: {
    redirect: {
      login: '/login',
      logout: '/login',
      home: false, // отключаем редирект куда либо после авторизации
      callback: '/login'
    },
    strategies: {
      local: {
        rewriteRedirects: true,
        // scheme: 'refresh',
        endpoints: {
          login: {
            url: '/auth/login'
          },
          // refresh: {
          //   url: '/auth/refresh/'
          // },
          logout: {
            // url: false
            url: '/auth/logout'
          },
          // user: false
          user: { url: '/auth/user', method: 'get' }
          // user: '/auth/user/'
        },
        token: {
          property: 'user.token',
          // type: false, // Bearer по умолчанию
          global: true, // токен автоматом встраивается во все axios запросы
          maxAge: 60 * 60 // максимальный срок жизни токена 1 часов
        },
        // refreshToken: {
        //   property: 'user.refresh_token',
        //   data: 'refresh_token'
        //   // maxAge: 60 * 60 * 24 * 30
        // },
        user: {
          // property: 'user.login',
          autoFetch: true
        },
        autoLogout: true
      }
    }
  },
  ssr: false, // Disable Server Side rendering
  target: 'server', // For server side rendering
  head: {
    titleTemplate: '%s - kb-front',
    title: 'База знаний',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    script: [
      { type: 'text/javascript', src: '//api.bitrix24.com/api/v1/' }, // bitrix библиотека
      { type: 'text/javascript', src: 'https://cdn.jsdelivr.net/npm/spark-md5' } // библиотека MD5 шифрования
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // ...
    'quill/dist/quill.core.css',
    // for snow theme
    'quill/dist/quill.snow.css',
    // for bubble theme
    'quill/dist/quill.bubble.css'
    // ...
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/quilEditor', ssr: false },
    { src: '~/plugins/vue-apexchart', ssr: false }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/toast',
    '@nuxtjs/auth-next'
  ],

  // модуль всплывающих сообщений
  toast: {
    position: 'top-center'
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: 'https://kbn.dglv.ru/api'
    // baseURL: 'http://localhost:8000/api'
  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    treeShake: true,
    theme: {
      dark: false,
      themes: {
        light: {
          accent: '#005BD5',
          primary: colors.yellow.darken1
        },
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
