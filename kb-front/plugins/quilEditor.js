// плагин QuillEditor для форматированных полей публикаций
import Vue from 'vue'
import VueQuillEditor from 'vue-quill-editor'

Vue.use(VueQuillEditor)
