// function parent (el) {
//   // вычисляем родителя отсекая последний элемент
//   const a = el.Kod.split('.') // из строки в массив
//   a.pop() // удаляем последний элемент
//   a.unshift(el.Razdel) // спереди добавляем букву раздела
//   return a.join('.') // обратно в строку
// }

function parent2 (el) {
  // вычисляем родителя отсекая последний элемент
  let res
  const len = el.system_object_id.length
  if (len === 4) {
    res = el.system_object_id[0] // X.XX.
  } else if (el.system_object_id[len - 2] === '.') {
    res = el.system_object_id.slice(0, -2) // вариант с "."
  } else { res = el.system_object_id.slice(0, -1) } // остальные варианты
  return res
}

// для сортировки
function compare (a, b) {
  if (a.system_object_id > b.system_object_id) { return 1 } // если первое значение больше второго
  if (a.system_object_id === b.system_object_id) { return 0 } // если равны
  if (a.system_object_id < b.system_object_id) { return -1 } // если первое значение меньше второго
}

// Требуемая глубина вывода ОКВЭД
const REQUIRED_DEEP = 7 // выбрано до уровня XX.XX группа
// E.38.32.43
// категория глубина 1
// XX класс глубина 4
// XX.X подкласс глубина 6
// XX.XX группа - глубина 7
// XX.XX.X подгруппа глубина 9
// XX.XX.XX вид глубина 10

// рекурсивная функция находит дочерние узлы
function searchChildren (parentIDX) {
  // фильтруем по нужной глубине ОКВЭД
  // debugger
  const res = this.filter(itemY => itemY.Kod && parent2(itemY) === parentIDX && itemY.system_object_id.length <= REQUIRED_DEEP)
    .sort(compare) // сортируем
    .map(item => ({
      id: item.Kod,
      name: item.Kod + ' ' + item.Name,
      children: searchChildren.call(this, item.Idx) // рекурсия
    }))
  // если одиночный элемент, то не выводим, исключение - 1-ый раздел
  if (parentIDX.length === 1 || res.length > 1) { return res }
}

function OkvedObj (obj) {
  // массив состоящий из главных разделов
  return obj.filter(item => !('Kod' in item))
    .sort(compare) // сортируем
    .map(itemM =>
      ({
        id: itemM.Idx,
        name: itemM.Razdel + ' - ' + itemM.Name,
        children: searchChildren.call(obj, itemM.Razdel)
      }))
}

export { OkvedObj }
