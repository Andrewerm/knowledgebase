import { VTextField } from 'vuetify/lib/components'
import BaseComponent from '../../components/categoryTemplates/BaseComponent'
import FieldFormattedEditor from '../../components/fieldCustomComponents/FieldFormattedEditor'
import FieldJsonTable from '../../components/fieldCustomComponents/FieldJsonTable'
import FieldTimeStampToDate from '../../components/fieldCustomComponents/FieldTimeStampToDate'
import MethodologyComponent from '@/components/categoryTemplates/MethodologyComponent'
import EducationComponent from '@/components/categoryTemplates/EducationComponent'
import ProductsComponent from '@/components/categoryTemplates/ProductsComponent'
import ConsultingComponent from '@/components/categoryTemplates/ConsultingComponent'
import CaseComponent from '@/components/categoryTemplates/CaseComponent'
import FieldDate from '@/components/fieldCustomComponents/FieldDate'
import LiteratureComponent from '@/components/categoryTemplates/LiteratureComponent'
import TechnologiesComponent from '@/components/categoryTemplates/TechnologiesComponent'
import OthersComponent from '@/components/categoryTemplates/OthersComponent'
import LaboratoryComponent from '@/components/categoryTemplates/LaboratoryComponent'

// Соответствие компонентов - полей. Тип поля берется из описания поля в модели. Если типа нет в списке, то по умолчание VTextField
const typeComponentSelect = {
  html: FieldFormattedEditor, // quillEditor
  json: FieldJsonTable, // JSON таблица
  timestamp: FieldTimeStampToDate, // timestamp преобразовывается в Дата/Время
  date: FieldDate // Датапикер
}

// базовый тип полей

function BaseFieldType (field, source, placeType) {
  this.field = field // имя поля
  this.source = source // ссылка на источник данных по статье
  this.placeType = placeType // локация на странице статьи
  const { type, label } = source.sourceColumns[field] // берем type и label из модели
  const result = {
    type,
    label,
    typeFieldComponent: typeComponentSelect[type] || VTextField, // компонент визуализации поля, пол умолчанию VTextField
    placeType// тип для местоположения в публикации
    // list - в списке всех полей, title - в заголовке, common - в карточке
  }
  Object.defineProperty(result, 'value', {
    // добавляем свойство value, которое получает геттер и сеттер
    get () {
      // если свойства нет, то создаём его (если новая статья)
      if (!(field in source.sourceDataTable)) {
        source.sourceDataTable[field] = undefined
      }
      return source.sourceDataTable[field]
    },
    set (newValue) {
      // проставляем свойство updated, оно потом пригодится для getSaveData
      source.sourceColumns[field].updated = true
      source.sourceDataTable[field] = newValue
    }
  })
  return result
}

const BaseType = Object.create(null, {
  sourceDataTable: { value: {}, writable: true }, // исходные данные статьи
  commonFields: { value: ['id', 'title_article', 'type_article', 'status_article', 'rating', 'created_at', 'updated_at', 'counter'] },
  sourceColumns: { value: null, writable: true }, //  информация о структуре данных в таблице
  getFilters: { value: [] },
  getSavedData: {
    // геттер собирает объект из свойств, в которых есть поле update
    get () {
      // создаём новый объект
      const result = Object.create(null)
      // объединяем поля общие и уникальные
      if (!('id' in this.sourceDataTable)) { // если ID не задан, то это новая статья
        result.type_article = this.sourceDataTable.type_article // тип статьи задан не через сеттер, поэтому заносим сами его в объект
        result.status_article = 1 // первоначальный тип черновик
        if (this.sourceDataTable.parent_article_id) result.parent_article_id = this.sourceDataTable.parent_article_id // в кейсах используется
      }
      const commonArrayFields = [...this.commonFields, ...this.uniqFields]
      commonArrayFields.forEach((item) => { // у кого сеттером проставлен updated, тот попадает в result объект
        if (item in this.sourceDataTable && 'updated' in this.sourceColumns[item]) {
          result[item] = this.sourceDataTable[item]
        }
      })
      return result
    }
  },
  tags: { value: {} },
  getFieldsForForm: {
    get () {
      // создаём новый объект
      const result = { tagsRules: this.tags } // добавляем информацию о правилах тэгов
      this.commonFields.forEach((item) => { // list - в списке всех полей, title - в заголовке, common - в карточке
        result[item] = new BaseFieldType(item, this, item === 'title_article' ? 'title' : 'common')
      })
      this.uniqFields.forEach((item) => {
        result[item] = new BaseFieldType(item, this, 'list')
      })
      // бежим по всем полям, определенным в данном типе статьи
      return result
    }
  },
  template: { value: BaseComponent },
  // проверка заполнены ли все обязательные поля статьи
  validated: {
    get () {
      return this.mandatoryFields.every(item => {
        return !!this.sourceDataTable[item]
      })
    }
  },
  mandatoryFields: { value: [] }
})
//
const ConsultingType = Object.create(BaseType, {
  uniqFields: {
    value: ['project_name', 'company', 'start_date', 'end_date', 'targets', 'description',
      'work', 'kpi', 'effect', 'project_manager', 'customer_members', 'experience_consulting', 'experience_school', 'experience_laboratory',
      'experience_digital']
  },
  // список обязательных полей для отправки на экспертизу
  mandatoryFields: {
    value: ['project_name', 'company', 'start_date', 'end_date', 'targets', 'description',
      'work', 'kpi', 'effect', 'project_manager', 'customer_members', 'experience_consulting', 'experience_school', 'experience_laboratory',
      'experience_digital'],
    enumerable: true
  },
  // правила какие тэги обязательны или нет для типа статьи
  // mandatory - обязательное
  // любой другой текст - необязательное
  // поля нет - не должно быть тэгов, которые не указаны
  tags: {
    value: {
      direction: 'mandatory',
      okved: 'mandatory',
      tools: 'optional',
      processes: 'mandatory'
    }
  },
  template: { value: ConsultingComponent }
})

const CaseType = Object.create(BaseType, {
  uniqFields: {
    value: ['problem', 'reasons', 'solution', 'before', 'actions', 'after', 'parent_article_id']
  },
  // список обязательных полей для отправки на экспертизу
  mandatoryFields: { value: ['problem', 'reasons', 'solution', 'before', 'actions', 'after', 'parent_article_id'], enumerable: true },
  tags: {
    value: {
      direction: 'mandatory',
      okved: 'mandatory',
      tools: 'mandatory',
      processes: 'mandatory',
      experience: 'mandatory'
    }
  },
  template: { value: CaseComponent }
})
const LiteratureType = Object.create(BaseType, {
  uniqFields: {
    value: ['author', 'description']
  },
  tags: {
    value: {
      tools: 'mandatory'
    }
  },
  template: { value: LiteratureComponent }

})
const Education = Object.create(BaseType, {
  uniqFields: {
    value: ['effect', 'targets', 'description', 'actions', 'duration', 'work', 'author', 'end_date', 'start_date', 'customer_members', 'count', 'solution']
  },
  tags: {
    value: {
      direction: 'mandatory',
      tools: 'mandatory',
      program: 'mandatory',
      employee: 'mandatory'
    }
  },
  template: { value: EducationComponent }
})
const Technologies = Object.create(BaseType, {
  uniqFields: {
    value: ['description', 'author']
  },
  tags: {
    value: {
      direction: 'mandatory'
    }
  },
  template: { value: TechnologiesComponent }
})
const Laboratory = Object.create(BaseType, {
  uniqFields: {
    value: ['company', 'targets', 'description', 'customer_members', 'work']
  },
  tags: {
    value: {
      direction: 'mandatory',
      okved: 'mandatory',
      tools: 'mandatory'
    }
  },
  template: { value: LaboratoryComponent }

})
const Methodology = Object.create(BaseType, {
  uniqFields: {
    value: ['description', 'targets', 'work', 'effect']
  },
  tags: {
    value: {
      direction: 'mandatory',
      tools: 'mandatory'
    }
  },
  template: { value: MethodologyComponent }
})
const Others = Object.create(BaseType, {
  uniqFields: {
    value: ['description']
  },
  template: { value: OthersComponent }
})
const Products = Object.create(BaseType, {
  uniqFields: {
    value: ['description', 'problem', 'work', 'effect', 'customer_members']
  },
  tags: {
    value: {
      direction: 'mandatory',
      tools: 'mandatory',
      packet: 'mandatory',
      type: 'mandatory'
    }
  },
  template: { value: ProductsComponent }
})
// соответствие типов статей шаблонам страниц
const accordance = {
  1: ConsultingType,
  2: CaseType,
  3: LiteratureType,
  4: Education,
  5: Technologies,
  6: Laboratory,
  7: Methodology,
  8: Others,
  9: Products
}

export default function ArticleObject (obj) {
  this.sourceDataTable = obj.rows[0]
  this.sourceColumns = obj.info.columns
  const typeArticle = obj.rows[0].type_article
  if (typeArticle in accordance) {
    Object.setPrototypeOf(this, accordance[typeArticle])// по типу статьи подключаем нужный прототип
    // eslint-disable-next-line no-console
  } else { console.error('Указанный тип ', typeArticle, 'не существует!') }
}
