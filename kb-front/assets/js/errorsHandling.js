// процедура обрабатывает ошибку получения данных с сервера во всех fetch в модулях
export default async function axiosErrorHandling (err, context) {
  context.$toast.error(`Проблемы ${err.message}`, { duration: 5000 })
  if (err.response.status === 401) { // если пользователь не авторизован то логинимся заново
    context.$auth.$storage.setLocalStorage('redirect', context.$route.path) // TODO должен сохраняться путь для обратного редиректа, но редиректа не происходит, видимо не так работает это
    await context.$auth.login() // если ошибка 401, то на страницу логина
  }
  console.error('error axiosErrorHandling', err, 'err.response.status', err.response.status)
}
