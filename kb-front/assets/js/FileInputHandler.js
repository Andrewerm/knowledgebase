// массив загружаемых файлов
const InputFilesArray = []

// проверка на существование в списке загружаемых файлов
function checkingFileAlreadyExists (hash) {
  return InputFilesArray.some(item => item.state === FileReader.DONE && item.hash === hash)
}

// конструктор объекта InputFileObject
export function InputFileObject (inputFile, hash = true) { // hash = true  - необхоимость вычислять хэш в моделе загрузки файло в хранилище
  if (inputFile) { // hash = false в модуле загрузки обложек для статей
    this.result = undefined // нужно для реактивности
    // отдельный FileReader для подсчёта хэша, потому что readAsArrayBuffer
    if (hash) {
      this._hashReader = new FileReader()
      this._hashReader.readAsArrayBuffer(inputFile)
    }
    // отдельный FileReader для преобразования в readAsDataURL
    this._reader = new FileReader()
    this._reader.readAsDataURL(inputFile)

    this.size = inputFile.size
    this.type = inputFile.type
    this.name = inputFile.name
    this.state = FileReader.LOADING // первоначальный статус
    this.loadingState = new Promise((resolve, reject) => {
      // ставим 2 слушателя, один на считывание readAsArrayBuffer и подсчёт хеша, другой на readAsDataURL, какой последний заканчивает, тот и возвращает статус готовности
      if (hash) {
        this._hashReader.addEventListener('loadend', () => {
        // eslint-disable-next-line no-undef
          this.hash = SparkMD5.ArrayBuffer.hash(this._hashReader.result) // TODO сделать поключение внешней библитеки не через CDN
          if (this.result) {
            if (!checkingFileAlreadyExists(this.hash)) {
              resolve(this._reader.readyState) // сообщаем родительскому компоненту о том, что данные готовы
            } else {
              reject(new Error(`файл ${this.name} уже есть под другим именем в данном списке и не будет загружен`))
            }
          }
        })
      }
      this._reader.addEventListener('loadend', () => {
        if (this._reader.error) { reject(new Error(`ошибка файла ${this._reader.error}`)) }
        this.result = this._reader.result
        if (hash && this.hash) {
          if (!checkingFileAlreadyExists(this.hash)) {
            resolve(this._reader.readyState) // сообщаем родительскому компоненту о том, что данные готовы
          } else {
            reject(new Error(`файл ${this.name} уже есть под другим именем в данном списке и не будет загружен`))
          }
        }
      }) // добавляем сразу event listener на окончание загрузки файла
    })
  } else this.result = 'error'
}

export default function filesHandler (listFiles) {
  InputFilesArray.length = 0 // очищаем массив (если это повторный выбор файлов)
  listFiles.forEach(item => {
    InputFilesArray.push(new InputFileObject(item))
  }
  )
  return InputFilesArray
}
