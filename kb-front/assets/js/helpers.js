const getNestedObject = function (object, path) {
  // let result=object.content
  const res = path.reduce(function (previousValue, item) {
    return previousValue.content[item]
  }, object)
  console.log('getNestedObject', object, path, res)
  return res
}

export { getNestedObject }
