import BaseTemplate from '../../components/filterTemplates/BaseTemplate'
import SimpleListTemplate
  from '../../components/filterTemplates/SimpleListTemplate'
import TagsFilterTemplate
  from '../../components/filterTemplates/TagsFilterTemplate'

// ID в диапазоне 1-999
function getRandomID () {
  return Math.floor(Math.random() * 1000)
}

const BaseFilter = Object.create(null, {
  title: { get () { return this.options.title } }, // заголовок фильтра
  viewComponent: { value: BaseTemplate },
  viewComponentParams: { // получение входных параметров для элемента фильтрации
    get () {
      return {
        title: this.title
      }
    }
  },
  initError: { // проверка инициализации компонента
    value () {
      if (!this.options) { return 'нужно задать options' }
      if (!this.options.title) { return 'нужно задать options.title ' }
    }
  },
  data: { value: [], writable: true }, // массив статей
  dataHandler: { // геттер/сеттер получения массива публикаций
    get () {
      return this.data
    },
    set (newValue) {
      this.data = newValue
    }
  },
  filterSelected: { value: [], writable: true }, // выбранные элементы фильтра
  updateRender: { // перерендеринг элемента списка путём перегенерации ключа
    value () {
      this.idFilterElement = getRandomID()
    }
  }
})

const SimpleList = Object.create(BaseFilter, {
  viewComponent: { value: SimpleListTemplate },
  dataHandler: { // геттер/сеттер получения массива публикаций
    get () {
      if (this.filterSelected.length) {
        return this.data.filter(item => this.filterSelected.map(item2 => item2.value).includes(item[this.options.field]))
      } else {
        return this.data
      }
    },
    set (newValue) {
      this.data = newValue
    }
  },
  viewComponentParams: { // получение входных параметров для элемента фильтрации
    get () {
      return {
        hint: `${this.title} не выбраны`,
        title: this.title,
        items: this.store.getters['guides/getForSimpleListFilter'](this.options.field)
          .map((item) => {
            item.disabled = !this.data.map(item2 => item2[this.options.field]).includes(item.value)
            return item
          })
      }
    }
  }

})

const TagsList = Object.create(BaseFilter, {
  viewComponent: { value: TagsFilterTemplate },
  tagGroups: { value: [], writable: true },
  tags: { value: [], writable: true },
  dataHandler: {
    get () {
      if (this.filterSelected.length) {
        const links = this.store.state.relatives.tags_articles.filter(item => this.filterSelected.includes(this.store.getters['guides/getTagByID'](item)))
        const linksArticleID = links.map(item => item.article_id)
        return this.data.filter(item => linksArticleID.includes(item.id))
      } else {
        return this.data
      }
    },
    set (newValue) {
      // получаем массив связей, которые используюся в наборе статей (ОКВЭД не берем)
      const links = this.store.state.relatives.tags_articles.filter(item => newValue.map(item2 => item2.id).includes(item.article_id))
      // получаем массив тэгов и оставляем только уникальные
      const tagsSet = new Set(links.map(item => this.store.getters['guides/getTagByID'](item)))
      this.tags = [...tagsSet] // в массив сохраняем все используемые тэги и ОКВЭДы
      // получаем массив имён категорий, которые используются в наборе данных, если string, то это ОКВЭД - 9  категория
      this.tagGroups = [...new Set(this.tags.map(item => this.store
        .getters['guides/getNameCategoryByID'](typeof item === 'string' ? 9 : item.category_id)))]
      this.data = newValue
    }
  }

})

// соответствие типов фильтров
const accordance = {
  simple: SimpleList,
  tags: TagsList
}

export default function FilterObject (type, store, options) {
  if (type in accordance) {
    this.store = store // доступ к vuex
    this.idFilterElement = getRandomID // присваиваем ID (для рекативности)
    this.options = options
    // debugger
    Object.setPrototypeOf(this, accordance[type])// по типу статьи подключаем нужный прототип
    if (this.initError()) { console.error(this.initError()) } else { console.info('фильтр ', this, ' инициализирован') }
    // eslint-disable-next-line no-console
  } else { console.error('Указанный тип ', type, 'не существует!') }
}
